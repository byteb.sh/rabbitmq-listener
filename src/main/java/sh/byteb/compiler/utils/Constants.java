package sh.byteb.compiler.utils;

public class Constants {

    public static final String EXEC_DIRECTORY = "/home/byteb/byte.sh/";
    public static final String JAVA_EXEC_DIRECTORY = EXEC_DIRECTORY + "JAVA/";
    public static final String PYTHON_EXEC_DIRECTORY = EXEC_DIRECTORY + "PYTHON";
    public static final String CPP_EXEC_DIRECTORY = EXEC_DIRECTORY + "CPP";

}
