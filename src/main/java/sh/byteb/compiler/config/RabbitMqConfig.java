package sh.byteb.compiler.config;

import org.springframework.amqp.core.*;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import sh.byteb.compiler.model.Language;

import java.util.Arrays;
import java.util.List;

@Configuration
public class RabbitMqConfig {

    @Bean("compilerExchange")
    TopicExchange compilerExchange() {
        return ExchangeBuilder
                .topicExchange("sh.byteb.compiler")
                .durable(true)
                .build();
    }


    @Bean("compilerJavaQueue")
    Queue javaQueue() {
        return QueueBuilder.durable(Language.JAVA.name().toLowerCase()).build();
    }

    @Bean("compilerCppQueue")
    Queue cppQueue() {
        return QueueBuilder.durable(Language.CPP.name().toLowerCase()).build();
    }

    @Bean("compilerPythonQueue")
    Queue pythonQueue() {
        return QueueBuilder.durable(Language.PYTHON.name().toLowerCase()).build();
    }


    @Bean("queueBindings")
    public List<Binding> marketDataBinding(@Qualifier("compilerJavaQueue") Queue javaQueue,
                                           @Qualifier("compilerCppQueue") Queue cppQueue,
                                           @Qualifier("compilerPythonQueue") Queue pythonQueue,
                                           @Qualifier("compilerExchange") TopicExchange exchange) {
        return Arrays.asList(
                BindingBuilder.bind(javaQueue).to(exchange).with(Language.JAVA.name().toLowerCase()),
                BindingBuilder.bind(cppQueue).to(exchange).with(Language.CPP.name().toLowerCase()),
                BindingBuilder.bind(pythonQueue).to(exchange).with(Language.PYTHON.name().toLowerCase())
        );
    }



}
