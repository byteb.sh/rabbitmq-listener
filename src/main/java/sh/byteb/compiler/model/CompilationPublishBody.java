package sh.byteb.compiler.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.util.StringUtils;

import java.util.UUID;

@JsonIgnoreProperties(ignoreUnknown = true)
public record CompilationPublishBody(CompilationRequest compilationRequest, String stdin, String code, String uuid) {

    @JsonCreator(mode = JsonCreator.Mode.PROPERTIES)
    public CompilationPublishBody(@JsonProperty("compilationRequest") CompilationRequest compilationRequest,
                                  @JsonProperty("stdin") String stdin,
                                  @JsonProperty("code") String code,
                                  @JsonProperty("uuid") String uuid) {
        this.compilationRequest = compilationRequest;
        this.stdin = stdin;
        this.code = code;
        this.uuid = StringUtils.hasText(uuid) ? uuid : UUID.randomUUID().toString();
    }

}
