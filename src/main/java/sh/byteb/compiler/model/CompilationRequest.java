package sh.byteb.compiler.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;



@JsonIgnoreProperties(ignoreUnknown = true)
public record CompilationRequest(@JsonProperty("language")
                                 Language language,

                                 @JsonProperty("maxMemory") @Min(value = 100, message = INVALID_MEMORY_VALUE)
                                 @Max(value = 2048, message = INVALID_MEMORY_VALUE) int maxMemory,

                                 @Min(value = 100, message = INVALID_RUNTIME_VALUE)
                                 @Max(value = 2048, message = INVALID_RUNTIME_VALUE) int timeLimit) {

    private final static int DEFAULT_MEMORY = 500;
    private final static int DEFAULT_RUNTIME = 2000;
    private final static String INVALID_MEMORY_VALUE = "Invalid max memory value";
    private final static String INVALID_RUNTIME_VALUE = "Invalid value for max runtime";
    private final static Language DEFAULT_LANGUAGE = Language.CPP;


    public CompilationRequest(Language language, int maxMemory, int timeLimit) {
        this.language = language;
        this.maxMemory = maxMemory;
        this.timeLimit = timeLimit;
    }

    public CompilationRequest(Language language) {
        this(language, DEFAULT_MEMORY, DEFAULT_RUNTIME);
    }

    public CompilationRequest() {
        this(DEFAULT_LANGUAGE, DEFAULT_MEMORY, DEFAULT_RUNTIME);
    }


}
