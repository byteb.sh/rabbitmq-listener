package sh.byteb.compiler.exception;

import java.util.UUID;

public class InternalRuntimeException extends RuntimeException {

    private final String reason;
    private final String title;
    private final int responseCode;
    private final String uuid;

    InternalRuntimeException(String reason, String title, int responseCode, String uuid) {
        this.reason = reason;
        this.title = title;
        this.responseCode = responseCode;
        this.uuid = uuid;
    }

    public InternalRuntimeException(String reason, String title, int responseCode) {
        this(reason, title, responseCode, UUID.randomUUID().toString());
    }


    public String getReason() {
        return reason;
    }

    public String getTitle() {
        return title;
    }

    public int getResponseCode() {
        return responseCode;
    }

    public String getUuid() {
        return uuid;
    }
}
