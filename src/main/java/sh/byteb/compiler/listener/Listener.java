package sh.byteb.compiler.listener;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.rabbitmq.client.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.support.AmqpHeaders;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.stereotype.Component;
import sh.byteb.compiler.executors.JavaExecutor;
import sh.byteb.compiler.model.CompilationPublishBody;


import java.io.IOException;


@Component("javaQueueListener")
public class Listener {

    private final ObjectMapper mapper;
    private static final String JAVA_QUEUE_NAME = "java";

    private final JavaExecutor javaExecutor;

    Logger log = LoggerFactory.getLogger(Listener.class);

    public Listener(ObjectMapper mapper, JavaExecutor javaExecutor) {
        this.mapper = mapper;
        this.javaExecutor = javaExecutor;
    }

    @RabbitListener(ackMode = "MANUAL", queues = JAVA_QUEUE_NAME, id = "rabbitMqListener")
    public void startToListen(Channel rabbitMqChannel, Message message, @Header(AmqpHeaders.DELIVERY_TAG) long messageTag) throws IOException, InterruptedException {
        CompilationPublishBody publishBody;
        try {
            publishBody = deserializePublishedMessage(message.getBody());
            log.info(javaExecutor.runCode(publishBody.code(), publishBody.stdin(), publishBody.compilationRequest().timeLimit()));
        } catch (Exception e) {
            log.error("Rejecting message. Cannot de-serialize message with content : ", message.getBody());
            rabbitMqChannel.basicReject(messageTag, false);
            return;
        }
        log.info("received message with id : {}", publishBody.uuid());
        log.info("received message with stdin : {}", publishBody.stdin());
        log.info("received message with code : {}", publishBody.code());

        rabbitMqChannel.basicAck(messageTag, true);
    }


    private CompilationPublishBody deserializePublishedMessage(byte[] rawMessageContent) throws IOException {
        return mapper.readValue(rawMessageContent, CompilationPublishBody.class);
    }


}
