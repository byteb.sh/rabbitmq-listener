package sh.byteb.compiler.executors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import sh.byteb.compiler.utils.Constants;

import java.io.File;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.concurrent.TimeUnit;

@Component("javaExecutor")
public class JavaExecutor {

    Logger log = LoggerFactory.getLogger(JavaExecutor.class);

    private final String CODE_FILE_SUFFIX = "Main.java";

    private final Path JavaExecutionDirectory = Paths.get(Constants.JAVA_EXEC_DIRECTORY);

    JavaExecutor() throws IOException {
        Files.createDirectories(JavaExecutionDirectory);
    }

    public String runCode(String code, String stdin, int timeLimit) throws IOException, InterruptedException {

        prepareExecDirectory("", code);

        var process = new ProcessBuilder("java", "Main.java").directory(JavaExecutionDirectory.toFile());


        var res = process.start();
        var inputToCode = new OutputStreamWriter(res.getOutputStream());
        inputToCode.write(stdin);

        res.waitFor(timeLimit, TimeUnit.MILLISECONDS);

        return new String(res.getInputStream().readAllBytes());
    }

    private void prepareExecDirectory(String uuid, String code) throws IOException {

        //Clearing out files from previous execution
        try (var walk = Files.walk(JavaExecutionDirectory, 2)) {
            walk.forEach(p -> {
                try {
                    if (Files.isRegularFile(p)) {
                        Files.delete(p);
                    }
                } catch (Exception e) {
                    log.error("Error occurred with clearing out execution space directory : ", e);
                }

            });

        }

        //Writing code to file
        Path codePath = Paths.get(Constants.JAVA_EXEC_DIRECTORY + CODE_FILE_SUFFIX);
        Files.createFile(codePath);
        Files.write(codePath, code.getBytes());

    }

}
